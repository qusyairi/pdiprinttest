﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;

namespace AstrumsHonda
{
    public partial class PrintingTest : Form
    {
        public PrintingTest()
        {
            InitializeComponent();
        }

        private void printServer_Click(object sender, EventArgs e)
        {
            printServer.Enabled = false;
            //Need to build XML string
            String requestXml = @"<jobsheet>
									<chasisno>6aa7db29-597f-4935-a011-9bff3cf3581b</chasisno>
									<engineno>2000000000</engineno>
									<colourcode>4000</colourcode>
									<model>6000</model>
									<variant>9000</variant>
									<currentstatus>ok</currentstatus>
									<defects>
										<defect>
											<no>1</no>
											<area>ADDITIONAL ACCESSORIES</area>
											<portion>RHS SIDE SILL</portion>
											<category>REVERSE SENSOR INSTALLATION</category>
											<defectlist>STOPPER NOT INSTALL</defectlist>
											<process>JIA</process>
											<rejectedby>Qusyairi</rejectedby>
											<repairby>Qusyairi 10/10/16 15:30PM</repairby>
										</defect>
										<defect>
											<no>2</no>
											<area>JIA</area>
											<portion>TYRE</portion>
											<category>REVERSE SENSOR INSTALLATION</category>
											<defectlist>ITEM SHORTAGE</defectlist>
											<process>JIA</process>
											<rejectedby>Qusyairi</rejectedby>
											<repairby>Qusyairi 10/10/16 15:30PM</repairby>
										</defect>
										<defect>
											<no>3</no>
											<area>RECONFIRM PROCESS</area>
											<portion>SWITCH</portion>
											<category>LOOSE PART INSTALLATION</category>
											<defectlist>WARNING LIGHT ON</defectlist>
											<process>ADDITIONAL ACCESSORIES</process>
											<rejectedby>Qusyairi</rejectedby>
											<repairby>Qusyairi 10/10/16 15:30PM</repairby>
										</defect>
									</defects>
								</jobsheet>";

            try{
                String printPath = "/print/jobSheet";
                String destinationUrl = "http://" + host.Text + ":" + port.Text + printPath;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(destinationUrl);
                byte[] bytes;
                bytes = System.Text.Encoding.ASCII.GetBytes(requestXml);
                request.ContentType = "text/xml; encoding='utf-8'";
                request.ContentLength = bytes.Length;
                request.Method = "POST";
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                HttpWebResponse response;
                response = (HttpWebResponse)request.GetResponse();
                if (response.StatusCode == HttpStatusCode.OK)
                {
                    Stream responseStream = response.GetResponseStream();
                    string responseStr = new StreamReader(responseStream).ReadToEnd();
                    MessageBox.Show("Printing message has been sent", "Printing Defect Job Sheet", MessageBoxButtons.OK, MessageBoxIcon.Asterisk, MessageBoxDefaultButton.Button1);
                    printServer.Enabled = true;
                    return;
                }else{
                    MessageBox.Show("Printing is not successful. Check Print Server.", "Printing Defect Job Sheet", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                }
                printServer.Enabled = true;
                return;

            }catch(Exception ex){
                printServer.Enabled = true;
                MessageBox.Show(ex.Message, "Printing Defect Job Sheet", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1);
                return;
            }
        }
    }
}