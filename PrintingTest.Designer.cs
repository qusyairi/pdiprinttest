﻿namespace AstrumsHonda
{
    partial class PrintingTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.printServer = new System.Windows.Forms.Button();
            this.port = new System.Windows.Forms.TextBox();
            this.host = new System.Windows.Forms.TextBox();
            this.host_label = new System.Windows.Forms.Label();
            this.port_label = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // printServer
            // 
            this.printServer.Location = new System.Drawing.Point(4, 236);
            this.printServer.Name = "printServer";
            this.printServer.Size = new System.Drawing.Size(233, 20);
            this.printServer.TabIndex = 4;
            this.printServer.Text = "Print Using Print Server";
            this.printServer.Click += new System.EventHandler(this.printServer_Click);
            // 
            // port
            // 
            this.port.Location = new System.Drawing.Point(59, 47);
            this.port.Name = "port";
            this.port.Size = new System.Drawing.Size(100, 23);
            this.port.TabIndex = 6;
            // 
            // host
            // 
            this.host.Location = new System.Drawing.Point(59, 18);
            this.host.Name = "host";
            this.host.Size = new System.Drawing.Size(178, 23);
            this.host.TabIndex = 7;
            this.host.TextChanged += new System.EventHandler(this.host_TextChanged);
            // 
            // host_label
            // 
            this.host_label.Location = new System.Drawing.Point(4, 18);
            this.host_label.Name = "host_label";
            this.host_label.Size = new System.Drawing.Size(49, 23);
            this.host_label.Text = "Host";
            this.host_label.ParentChanged += new System.EventHandler(this.label1_ParentChanged_2);
            // 
            // port_label
            // 
            this.port_label.Location = new System.Drawing.Point(4, 47);
            this.port_label.Name = "port_label";
            this.port_label.Size = new System.Drawing.Size(49, 23);
            this.port_label.Text = "Port";
            this.port_label.ParentChanged += new System.EventHandler(this.label1_ParentChanged_3);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(100, 20);
            this.label2.Text = "Print Server";
            this.label2.ParentChanged += new System.EventHandler(this.label2_ParentChanged);
            // 
            // PrintingTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(240, 268);
            this.Controls.Add(this.port_label);
            this.Controls.Add(this.host_label);
            this.Controls.Add(this.host);
            this.Controls.Add(this.port);
            this.Controls.Add(this.printServer);
            this.Controls.Add(this.label2);
            this.Menu = this.mainMenu1;
            this.Name = "PrintingTest";
            this.Text = "Honda PDI Astrums";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button printServer;
        private System.Windows.Forms.TextBox port;
        private System.Windows.Forms.TextBox host;
        private System.Windows.Forms.Label host_label;
        private System.Windows.Forms.Label port_label;
        private System.Windows.Forms.Label label2;

    }
}

